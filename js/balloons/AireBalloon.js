/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    
], function () {
    function getBalloon(feature, datos) {
        var etiqueta = "", etiquetaDatos="";
        var _etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                 '$DATOS' +
                '</table>' +
                '</div>';
        var _etiquetaDatos = '<tr bgcolor="$COLOR">' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                '</tr>';
        //var name = "Transmision en vivo";
        var properties = feature.properties;
        var i=0, color;
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        var actualTime = h+":"+m;
        if(properties && !datos) {
            for (var key in properties) {
                if(i%2 === 0 )
                    color = '#19232c';
                else
                    color = '';
                i++;
                if(key === "Hora") 
                    properties[key] = actualTime;
                etiquetaDatos+= _etiquetaDatos.replace("$Propertie", key+":").replace("$Value", properties[key]).replace("$COLOR", color);
            }
        }
        
        //var data = getActualDataAmbiental();
        for(var key in datos) {
            
            //for(var j=0; j<parametros[i].length; j++) {
                if(i%2 === 0 )
                    color = '#19232c';
                else
                    color = '';
                etiquetaDatos+= _etiquetaDatos.replace("$Propertie", key+":").replace("$Value", datos[key]).replace("$COLOR", color);
                i++;
            //}
        }
        etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos);
        var balloon;
        //balloon = etiqueta.replace('$NAME', name);
        //console.log(feature.properties.name);
        return etiqueta;   
    }
    
    return  { getBalloon: getBalloon };
});


