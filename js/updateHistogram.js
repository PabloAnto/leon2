/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var datos={}, datosS={};
define([
    'luciad/util/Promise',
    'luciad/shape/ShapeFactory',
    "time/TimeSlider",
    "time/TimeSliderWithOverview",
    'luciad/util/ColorMap',
    'luciad/transformation/TransformationFactory',
    
    "recursos/js/Graficas/Graficas",
    "./timeChartLayer",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    
    
    'luciad/view/feature/ParameterizedLinePainter',
    'luciad/util/ColorMap',
    'luciad/view/feature/ShapeProvider',
    "recursos/js/Util"
    //"./painters/EventTimePainter"
], function (Promise, ShapeFactory, TimeSlider,TimeSliderWithOverview,ColorMap,TransformationFactory,
            Graficas, timeChartLayer, MemoryStore, FeatureModel, FeatureLayer,
            ParameterizedLinePainter, ColorMap, ShapeProvider, Util) {
    
    var timeSlider, nL=0, columnsMonthLayer, columnsDayLayer=[];
     /*
     * updateHistogram, esta funcion se encarga de actualizar toda la informacion necesario
     * al mover la linea de tiempo
     * @returns {undefined}
     */
    var timeFilter;
    function updateHistogram(map, features, layer, reference, startTime, endTime, tablaDatos, graficas, filtrosOptions, keyEventTime) {
        
        var tablaActual;
        start();
        function start() {
            var shapeProvider = new ShapeProvider();
            shapeProvider.reference = TimeSlider.REFERENCE;
            shapeProvider.provideShape = function(feature) {
                if(!feature.properties.EventTime) {
                    var time = feature.properties[keyEventTime];
                    feature.properties.EventTime = time;
                }
                var date = feature.properties.EventTime;
                var time = Date.parse(date) / 1000 | 0;
                var depth = 100;
                return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
            };
            var timeEventLayer = new FeatureLayer(layer.model, {
                selectable: true,
                shapeProvider: shapeProvider
                //painter: new EventTimePainter()
            });
            var nDatos = 0;
            var updateTimeFilters = function(actualizarDatos) {
                layer.filter = null;
                var mapBounds = timeSlider.mapBounds;
                var filterStart = (timeSlider.mapBounds.x | 0 );
                var filterWidth = (timeSlider.mapBounds.width | 0 );
                var filterEnd = filterStart + filterWidth;
                var values=[], j=0;
                var Labels = filtrosOptions.labels;
                try{
                for(j=0;j<Filtros.length;j++) {
                    if(Filtros[j] && Filtros[j]!=="") {
                        values[j] = Filtros[j].selectedOptions[0].value;
                    }
                }
                }catch(e) {
                     console.log(e);
                     return null;
                }
                datos = {conteo: 0, monto: 0};
                datosS = {};
                nDatos = 0;
                    layer.filter = function(feature) {
                        var f = timeEventLayer.shapeProvider.provideShape(feature);
                        var vFeatures=[];
                        nDatos++;
                        for(j=0;j<Filtros.length;j++) {
                            var idLabel;
                            if(typeof Labels[j] === "string")
                                idLabel = Labels[j];
                            vFeatures[j] = feature.properties[idLabel]+"";
                        }
                        var xFiltros = 0;
                        if(mapBounds.contains2D(f)) {
                            for(j=0;j<Filtros.length;j++) {
                                if(vFeatures[j] === values[j] || values[j] === "Todos") {
                                    xFiltros++;
                                }
                            }
                            if(xFiltros === Filtros.length){
                                if(nDatos <= 1100) {
                                    var ageb = feature.properties.CVE_AGEB;
                                    if(ageb) {
                                        if(!datos[ageb]) {
                                            datos[ageb] = {conteo: 0, monto: 0};
                                        }
                                        datos[ageb].conteo ++;
                                        var monto = parseInt(feature.properties["Monto Apro"]);
                                        datos[ageb].monto += monto;
                                    }
                                    
                                    var sector = feature.properties.SECCION;
                                    if(sector) {
                                        if(!datosS[sector])
                                            datosS[sector] = 0;
                                        datosS[sector] ++;
                                    }
                                }
                                return true;
                            } else
                                return false;
                        } else
                            return false;
                    };
                    actualizarDatos = false;
                try {
                    tablaActual = Graficas.actualizarTabla(tablaDatos,timeEventLayer, mapBounds);
                    for(var k =0; k<graficas.length;k++) {
                        Graficas.actualizarGraficaIdTexto(graficas[k].id, tablaActual, graficas[k].tipo, graficas[k].label, graficas[k].top);
                    }
                    //document.getElementById("nEventosA").value = nuevaTabla.length;
                    //crearTablaDIV("tablaActual", nuevaTabla);
                    
                }catch(e) {
                    console.log("Error al actualizar grafica");
                }
                
            }; /// fin UpdateFilters
            var filtrosl = filtrosOptions.filtros, Filtros=[];
            for(var fi in filtrosl) {
                Filtros[fi] = document.getElementById( filtrosl[fi] );
                Filtros[fi].addEventListener( "change", function () {
                    updateTimeFilters(true);
                });
            }
            
            
            
            if(!timeSlider)
                timeSlider = new TimeSliderWithOverview("timeSlider", "timeSliderOverview", startTime, endTime, null, null);

            columnsMonthLayer = timeChartLayer.createMounthLayer(features, startTime, endTime);
            if(columnsMonthLayer) 
                timeSlider.layerTree.addChild(columnsMonthLayer);
            
            
            /*columnsDayLayer = timeChartLayer.createDaysLayer(features, startTime, endTime);
            if(columnsDayLayer) {
                timeSlider.layerTree.addChild(columnsDayLayer);
            }*/
            //columnsDayLayer[nL] = TimeLineLayer.createLayer(features, "hours", 12*30, startTime, endTime, e);
            //timeSlider.layerTree.addChild(columnsDayLayer[nL]);

            timeSlider.refit();

            var transformation = TransformationFactory.createTransformation(map.reference, reference);
            var histogramUpdater = function(accumulate) {
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    for (var i = 0; i < features.length; i++) {
                        if(lonlatBounds.contains2DPoint(features[i].shape)) {
                        var date = features[i].properties.EventTime;
                            var time = Date.parse(date) / 1000 | 0;
                            var magnitude = features[i].properties.mag;
                            accumulate(time, magnitude);
                        }
                    }
                };
            timeSlider.getHistogram().updateHistogram(histogramUpdater);
            
            map.on("idle", function() {
                timeSlider.getHistogram().updateHistogram(histogramUpdater);
            });
            map.on("MapChange", function() {
                var lonlatBounds = transformation.transformBounds(map.mapBounds);
                //console.log(lonlatBounds);
                if (timeFilter) {
                    timeEventLayer.filter = function(feature) {
                    // var time = offset + feature.properties.time / 1000 | 0;
                        var date = feature.properties.EventTime;
                        var time = Date.parse(date) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape);
                        }
                        else {
                            return false;
                        }
                    };
                }
                else {
                    timeEventLayer.filter = function(feature) {
                        return lonlatBounds.contains2DPoint(feature.shape);
                    };
                }
            });

            timeSlider.on("MapChange", function() {
                    updateTimeFilters(true);
                /*var left = timeSlider.mapBounds.left;
                var right = timeSlider.mapBounds.right;
                var mid = ( left + right ) / 2;
                console.log("left "+left+" right "+right+" mid "+mid);
                left = new Date(left);
                right = new Date(right);
                mid = new Date(mid);
                console.log("left "+left+" right "+right+" mid "+mid);
                */
            });
        
        //Fin Promise
        //});
        }
        ////================================================================================////////
        $("#verTablasAccidentes").click(function () {
            document.getElementById("LabelTabla1").innerHTML = "Datos Accidentes";
            var tablaCreada = Util.crearTablaDIV("tablaCompleta1", tablaDatos);
            document.getElementById("LabelTabla2").innerHTML = "Datos Dentro del rango de tiempo";
            Util.crearTablaDIV("tablaCompleta2", tablaActual);
            $("#panelTablas").fadeIn("slow");
        });
    //fin UpdateHistogram
    }
    /*
     *============================================================================================================
     *                                      Actual Date
     *============================================================================================================
     */
    function getActualDate() {
        if(!timeSlider || !timeSlider.mapBounds)
            return null;
        var left = timeSlider.mapBounds.left*1000, right = timeSlider.mapBounds.right*1000;
        var leftDate = Util.formatDate(left), rightDate = Util.formatDate(right);
        var middle = ((right - left)/2) + left;
        var middleDate = Util.formatDate(middle);
        return {middle:middle, left: left, right: right,
            middleDate: middleDate, leftDate: leftDate, rightDate: rightDate};
    }
    
    
    return {
        updateHistogram: updateHistogram,
        timeSlider: timeSlider,
        getActualDate: getActualDate
    };
});

function getDataAgeb(ageb) {
    return datos[ageb] || {};
}

function getDataSector(sector) {
    return datosS[sector] || 0;
}
