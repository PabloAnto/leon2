/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samples/common/IconFactory',
        'luciad/util/ColorMap'
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap) {
    
    function layerPainter() {
        this.regularStyle = {
            url: "../../proyecto/recursos/icons/marca.png",
            width: "50px",
            height: "50px",
            draped: false
        };
        this.densityStyle = {
            width: "40px",
            height: "40px",
            draped: true
        };
        this.defaultStyle = { 
                    fill: {color:  Azul},
                    stroke: {
                        color: sGris,
                        width: 2} 
                };
        this.selectStyle = { 
                    fill: {color:  Verde},
                    stroke: {
                        color: sRojo ,
                        width: 2} 
                };
        this.styleP = {
            url: "data/icons/presentation.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleA = {
            url: "data/icons/water.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleU = {
            url: "data/icons/skyline.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleV = {
            url: "data/icons/house.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleE = {
            url: "../recursos/icons/lugares/iconEscuela.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleT = {
            url: "data/icons/school-bus.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleD = {
            url: "data/icons/football.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleS = {
            url: "../recursos/icons/lugares/iconHospital.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleC = {
            url: "data/icons/travel-guide.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        this.styleSe = {
            url: "data/icons/shield.png",
            width: "30px",
            height: "30px",
            draped: false
        };
        
        var iconverde = IconFactory.circle({stroke: sVerde, fill: Verde, width: 30, height: 30});
        var iconamarillo = IconFactory.circle({stroke: sAmarillo, fill: Amarillo, width: 30, height: 30});
        var iconnaranja = IconFactory.circle({stroke: sNaranja, fill: Naranja, width: 30, height: 30});
        var iconrojo = IconFactory.circle({stroke: sRojo, fill: Rojo, width: 30, height: 30});
        this.sVerde = {
            width: "30px",
            height: "30px",
            image: iconverde,
            draped: false
        };
        this.sAmarillo = {
            width: "30px",
            height: "30px",
            image: iconamarillo,
            draped: false
        };
        this.sRojo = {
            width: "30px",
            height: "30px",
            image: iconrojo,
            draped: false
        };
        this.sNaranja = {
            width: "30px",
            height: "30px",
            image: iconnaranja,
            draped: false
        };
        this.sVerde2 = {
            width: "40px",
            height: "40px",
            image: iconverde,
            draped: false
        };
        this.sAmarillo2 = {
            width: "40px",
            height: "40px",
            image: iconamarillo,
            draped: false
        };
        this.sRojo2 = {
            width: "40px",
            height: "40px",
            image: iconrojo,
            draped: false
        };
        this.sNaranja2 = {
            width: "40px",
            height: "40px",
            image: iconnaranja,
            draped: false
        };
        
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 900000,1 / 4000000, 1/900000, 1/50000, 1/90000, 1/50000];
    };
    
    var sAmarillo = "rgb( 252, 238, 0 )", Amarillo = "rgba( 252, 238, 0, 0.5 )";
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    var x = 1, y = 0;
    x = Math.round(parseFloat($("#Densidad").val())) || 2;
    var gradient = ColorMap.createGradientColorMap([
                {level: y, color: "rgba(  0,   0,   255, 1.0)"},
                {level: y+=x, color: "rgba(  0, 125,   255, 1.0)"},
                {level: y+=x*2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: y+=x*3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: y+=x*4, color: "rgba(255, 0, 0, 1.0)"}
                ]);
    layerPainter.prototype.density = {
         colorMap: gradient
    };
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        
        var style = state.selected ? this.regularStyle :
                (layerPainter.prototype.density ? this.densityStyle : this.regularStyle);
        var x = Math.round(parseFloat($("#Densidad").val()));
        if( x <= 10 && state.selected === false) {
           style = this.densityStyle ;
            geoCanvas.drawIcon(shape, style);
        } else {
            var type = feature.properties.Categoria;
            switch(type) {
                case "Proyecto de inversi�n":
                case "Proyecto de inversión": style = this.styleP; break;
                case "Agua y saneamiento": style = this.styleA; break;
                case "Urbanizaci�n":
                case "Urbanización": style = this.styleU; break;
                case "Vivienda": style = this.styleV; break; 
                case "Educación":
                case "Educaci�n": style = this.styleE; break;
                case "Transporte y vialidades": style = this.styleT; break;
                case "Deporte": style = this.styleD; break;
                case "Salud": style = this.styleS; break;
                case "Cultura y turismo": style = this.styleC; break;
                case "Seguridad": style = this.styleSe; break;
                default: style = this.regularStyle; break;
            }
            geoCanvas.drawIcon(shape, style);
            var circle1, circle2;
            var actualTime = getActualTimeObras();
            var inicio = Date.parse(feature.properties["Fecha de I"]);
            var termino = Date.parse(feature.properties["Fecha de t"]);
            
            if(actualTime < inicio)
                circle1 = this.sVerde2;
            if(actualTime > inicio && actualTime <= termino) 
                circle1 = this.sAmarillo2;
            if(actualTime > termino)
                circle1 = this.sRojo2;
            geoCanvas.drawIcon(shape, circle1);
        }
        
    };
    
 
    return layerPainter;
});

