/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
    "luciad/view/style/PathLabelPosition",
    "luciad/view/style/PathLabelRotation",
        'samples/common/IconFactory'
    ], function (FeaturePainter, PathLabelPosition, PathLabelRotation, IconFactory) {
    
    function layerPainter() {
        var verde = IconFactory.circle({stroke: sVerde, fill: Verde, width: 30, height: 30});
        var amarillo = IconFactory.circle({stroke: sAmarillo, fill: Amarillo, width: 30, height: 30});
        var naranja = IconFactory.circle({stroke: sNaranja, fill: Naranja, width: 30, height: 30});
        var rojo = IconFactory.circle({stroke: sRojo, fill: Rojo, width: 30, height: 30});
        this.style = {
            width: "20px",
            height: "20px",
            image: "data/icons/garbage.png",
            draped: false
        };
        this.styleV = {
            width: "30px",
            height: "30px",
            image: verde,
            draped: false
        };
        this.styleA = {
            width: "30px",
            height: "30px",
            image: amarillo,
            draped: false
        };
        this.styleR = {
            width: "30px",
            height: "30px",
            image: rojo,
            draped: false
        };
        this.styleN = {
            width: "30px",
            height: "30px",
            image: naranja,
            draped: false
        };
        this.styleV2 = {
            width: "40px",
            height: "40px",
            image: verde,
            draped: false
        };
        this.styleA2 = {
            width: "40px",
            height: "40px",
            image: amarillo,
            draped: false
        };
        this.styleR2 = {
            width: "40px",
            height: "40px",
            image: rojo,
            draped: false
        };
        this.styleN2 = {
            width: "40px",
            height: "40px",
            image: naranja,
            draped: false
        };
        this.labelStyle = {
            positions: PathLabelPosition.ABOVE,
            //priority: poblacion,
            rotation: PathLabelRotation.FIXED_LINE_ANGLE
        };
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 500000,1 / 250000];
    };
    
    var sAmarillo = "rgb( 252, 238, 0 )", Amarillo = "rgba( 252, 238, 0, 0.5 )";
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb( 255, 158, 0 )', Naranja = 'rgba( 255, 158, 0 , 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        geoCanvas.drawIcon(shape, this.style);
        
        var id = feature.properties.Name;
        var datos = getDatosContenedores(id);
        if(datos !== null) {
            var style;
            var estado = datos.estadopeso;
            switch(estado) {
                case "verde": style = this.styleV; break;
                case "amarillo": style = this.styleA; break;
                case "naranja": style = this.styleN; break;
                case "rojo": style = this.styleR; break;
            }
            geoCanvas.drawIcon(shape, style);
            var estado2 = datos.estadovolumen, style2;
            switch(estado2) {
                case "verde": style2 = this.styleV2; break;
                case "amarillo": style2 = this.styleA2; break;
                case "naranja": style2 = this.styleN2; break;
                case "rojo": style2 = this.styleR2; break;
            }
            geoCanvas.drawIcon(shape, style2);
        }
        
    };
    
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            if(properties.Name) {
                labelName = properties.Name;
            }
            else {
            for(var key in properties) {
                if(i===0)
                    labelName = properties[key];
                i++;
            } 
            }
        } else {
            labelName = feature.id;
        }
        
        label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        if(state.level > 3) {
            if(layer.label === "Luminarias Tramos") {
                labelCanvas.drawLabelOnPath(label.replace("$color", sBlanco),shape, this.labelStyle);
            } else {
            if(state.selected)
                labelCanvas.drawLabel(label.replace("$color", sRojo),shape, {});
            else
                labelCanvas.drawLabel(label.replace("$color", sBlanco),shape, {});
            }
        }
    };
 
    return layerPainter;
});

