/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samples/common/IconFactory',
        "luciad/view/style/PointLabelPosition"
    ], function (FeaturePainter, ShapeFactory, IconFactory, PointLabelPosition) {
    
    function layerPainter() {
        this.Nivel0 = { 
            fill: {color: Verde} 
        };
        this.Nivel1 = { 
            fill: {color: Amarillo} 
        };
        this.Nivel2 = { 
            fill: {color: Naranja} 
        };
        this.Nivel3 = { 
            fill: {color: Rojo} 
        };
        this.sinDefinir = {
            fill: {color: Gris}
        };
        
        this.jitnivel1 = { 
            fill: {color: nivel1} 
        };
        this.jitnivel2 = { 
            fill: {color: nivel2} 
        };
        this.jitnivel3 = { 
            fill: {color: nivel3} 
        };
        this.jitnivel4 = { 
            fill: {color: nivel4} 
        };
        this.jitnivel5 = { 
            fill: {color: nivel5} 
        };
        this.jitnivel6 = { 
            fill: {color: nivel6} 
        };
        this.jitnivel7 = { 
            fill: {color: nivel7} 
        };
        this.labelStyle ={
            positions: (PointLabelPosition.CENTER),
            offset: 5,
            priority : 2
        };
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 500000,1 / 250000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)';
    var Amarillo = "rgba( 252, 238, 19, 0.5 )";
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(130,130,130, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    var Valor=0;
    var nivel1 = "rgba(0,0,131,0.5)", nivel2 = "rgba(1,93,184,0.5)", nivel3 = "rgba(5,239,248,0.5)";
    var nivel4 = "rgba(210,245,46,0.5)", nivel5 = "rgba(253,170,0,0.5)", nivel6 = "rgba(250,0,0,0.5)";
    var nivel7 = "rgba(128,0,0,0.5)";
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        
        
        
        var shape3d;
        if(feature.properties.maximumHeight) {
            if(feature.properties.minimumHeight)
                shape3d = ShapeFactory.createExtrudedShape(
                    shape.reference, shape, feature.properties.minimumHeight, feature.properties.maximumHeight);
            else
                shape3d = ShapeFactory.createExtrudedShape(
                    shape.reference, shape, 0, feature.properties.maximumHeight);
        }
        var ageb = feature.properties.CVE_AGEB;
        var style, datos = getDataAgeb(ageb);
        var valor = datos.conteo || 0;
        var factor = document.getElementById("filtroAgebs").selectedOptions[0].value;
        var porcentaje;
        if(factor ==="0") {
            valor = datos.conteo || 0;
            porcentaje = (valor / 13) *100;
        }
        if(factor === "1") {
            valor = datos.monto || 0;
            porcentaje = (valor / 4000000) *100;
        }
        
        /*if(!porcentaje)
            style = this.sinDefinir;
        if(porcentaje >0 && porcentaje <= 25)
            style = this.nivel0;
        if(porcentaje > 25 && porcentaje <= 50)
            style = this.nivel1;
        if(porcentaje > 50 && porcentaje <= 75)
            style = this.nivel2;
        if(porcentaje > 75)
            style = this.nivel3;
        */
        Valor = porcentaje.toFixed(2);
        if(!porcentaje)
            style = this.sinDefinir;
        if(porcentaje >0 && porcentaje <= 15)
            style = this.jitnivel1;
        if(porcentaje > 15 && porcentaje <= 30)
            style = this.jitnivel2;
        if(porcentaje > 30 && porcentaje <= 45)
            style = this.jitnivel3;
        if(porcentaje > 45 && porcentaje <= 60)
            style = this.jitnivel4;
        if(porcentaje > 60 && porcentaje <= 75)
            style = this.jitnivel5;
        if(porcentaje > 75 && porcentaje <= 90)
            style = this.jitnivel6;
        if(porcentaje > 90) 
            style = this.jitnivel7;
        //if(state.level <=4) {
            geoCanvas.drawShape(shape3d? shape3d : shape, style);
        /*} else {
            geoCanvas.drawShape(shape3d? shape3d : shape,{ 
                stroke: {
                    color: "rgba( 119, 191, 255, 0.7)",
                    width: 5} 
            });
        }*/
        
    };
    
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            if(properties.NOMMUN) {
                labelName = properties.NOMMUN;
            }
            else {
            for(var key in properties) {
                if(i===0)
                    labelName = properties[key];
                i++;
            } 
            }
        } else {
            labelName = feature.id;
        }
        label  = '<div class="labelwrapper">' +
                                '<div class="sensorLabel blueColorLabel">' +
                                '<div class="theader">' +
                                '<div class="leftTick blueColorTick"></div>' +
                                '<div class="rightTick blueColorTick"></div>' +
                                '<div class="name">'+labelName+'</div>' +
                                '</div>' +
                                '<div class="type">Porcentaje: '+Valor+'</div>' +
                                '</div>' +
                                '</div>';
        //label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        if(state.level > 3) {     
            if(state.selected)
                labelCanvas.drawLabel(label.replace("$color", sRojo),shape, this.labelStyle);
            else
                labelCanvas.drawLabel(label.replace("$color", sBlanco),shape, this.labelStyle);
        }
    }
 
    return layerPainter;
});

