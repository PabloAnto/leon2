var map, actualTime, actualTimeObras, datosContenedores, datosEstaciones;

define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    
    
    "luciad/reference/ReferenceProvider",
    "samples/common/LayerConfigUtil",
    "recursos/js/Util",
    
    "./TimeChartManager",
    "luciad/view/LayerType",
    
    "./painters/EstadoPainter",
    "./painters/SemaforosPainter",
    "./painters/ContenedoresPainter",
    "./painters/OrigenDestinoPainter",
    "./painters/heatPainter",
    
    "./painters/CamionPainter",
    "./painters/AgebPainter",
    "./painters/SemaforosCallesPainter",
    "./painters/SeccionesPainter",
    "./painters/EstacionesPainter",
    
    "./balloons/AgebBalloon",
    "./balloons/SeccionBalloon",
    "recursos/js/Graficas/Graficas",
    "./updateHistogram",
    //'luciad/util/Promise',
    'luciad/util/ColorMap',
    
    
    "./balloons/AireBalloon",
    
    
    "./balloons/DefaultBalloon",
    "recursos/js/Shapes",
    "./Tablas",
    "./GraficasOptibus"
    
    //"./OnHoverController"
    
], function (MapFactory, GoogleMap, LayerFactory,
         ReferenceProvider, LayerConfigUtil, Util, TimeChartManager, LayerType,
        EstadoPainter, SemaforosPainter, ContenedoresPainter, OrigenDestinoPainter,heatPainter,
        CamionPainter, AgebPainter, SemaforosCallesPainter, SeccionesPainter, EstacionesPainter,
        AgebBalloon, SeccionBalloon, Graficas, updateHistogram,  ColorMap, AireBalloon, 
        DefaultBalloon, Shapes, Tablas, GraficasOptibus ) {
    console.log("Iniciando...");
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    //var userName = document.getElementById("userName").innerHTML;
    var conectToFusion = false;
    var tipoUsuario = document.getElementById("userType").innerHTML;
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: false,
                    includeMouseLocation: false});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: true, includeBingLayer: ["Road", "Aerial"], openLayerControl: false});
        }
        //var hover = new OnHoverController();
        //map.controller = hover;
        CrearCapas();
    }
    
    var guanajuato, eventos={}, limpiaUbicaciones={}, limpiaSectores=[];
    var limpiaRutas={}, optibusRuta={}, optibusRuta2={}, semaforos={}, obrasPublicas={}, agebs={}, limpiaRutasTR={}, limpiaCamiones={};
    var semaforosCalles={}, optibusView={}, rutas=[], secciones={}, optibusEstaciones={};
    var tablaSensores=[];
    
    function CrearCapas() {
        LayerConfigUtil.addLonLatGridLayer(map);
        var mexicoLayer;
        if(conectToFusion === false) {
            mexicoLayer = LayerFactory.createKmlLayer({label: "Mexico", selectable: false, layerType: LayerType.BASE}, '../../proyecto/recursos/estados.kml');
        } else {
            mexicoLayer = LayerFactory.createWMSLayer(referenceC, {label: "Mexico", selectable: false, layerType: LayerType.BASE}, "http://localhost:8081/ogc/wms/estadosmexicojson", "estadosmexicojson");
        }
        map.layerTree.addChild(mexicoLayer, "bottom");
        Util.fitToMexico(map, true);
        
        guanajuato = LayerFactory.createWMSLayer(referenceC, {label: "Guanajuato (WMS)", selectable: true, layerType: LayerType.BASE, maxScale: 0.0000022964002584029857}, "https://lfci.aggme.tech:/ogc/wms/guanajuatomunicipios", "guanajuatomunicipios");
        map.layerTree.addChild(guanajuato, "above", mexicoLayer);
        
        
        // ================================ limpia ===================================== //
        if(tipoUsuario === "dev" || tipoUsuario === "limpia") {
            limpiaUbicaciones = LayerFactory.createFeatureLayer(referenceC, {label: "Origen Destino", layerType: LayerType.STATIC,
                painter: new OrigenDestinoPainter(), selectable: true, id:"Limpia0"}, "data/LIMPIA_PUBLICA2/PARTIDA_LLEGADA_LIMPIAPUB.geojson");
            map.layerTree.addChild(limpiaUbicaciones);
            limpiaUbicaciones.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };
            
            for(var i=0; i<=6; i++) {
                limpiaSectores[i] = LayerFactory.createFeatureLayer(referenceC, {label: "Contenedores Seccion "+i, layerType: LayerType.STATIC,
                    painter: new ContenedoresPainter(), selectable: true, id:"LSector"+i}, "data/LIMPIA_PUBLICA2/LPsector"+i+".geojson");
                map.layerTree.addChild(limpiaSectores[i]);
                
            }
            limpiaSectores.forEach(function(layer) {
                layer.balloonContentProvider = function (feature) {
                    var datos = getDatosContenedores(feature.properties.Name);
                    return AireBalloon.getBalloon(feature, datos);
                };
            });
            limpiaRutas = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia View", layerType: LayerType.DINAMIC, selectable: true,
                /*painter: new CamionPainter(),*/ id: "LRutas"}, 
                {/*dataSetStartTime: Date.parse("2019/12/14 08:00"), dataSetEndTime: Date.parse("2019/12/14 18:00"),*/ 
                    glowColor: "rgb( 255, 62, 0)", lineWidth: 8}, "data/LIMPIA_PUBLICA2/nueva.geojson", rutasHandeler);
            //for(i=1; i<=6; i++) {
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r1m", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas1"}, { glowColor: "rgb( 255, 62, 0)", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r1m.geojson", function(layer) {rutas[0] = layer});
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r1t", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas2"}, { glowColor: "rgb( 255, 62, 0)", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r1t.geojson", function(layer) {rutas[1] = layer});
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r2m", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas3"}, { glowColor: "rgb( 255, 62, 0)", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r2m.geojson", function(layer) {rutas[2] = layer});
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r2t", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas4"}, { glowColor: "rgb( 255, 62, 0)", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r2t.geojson", function(layer) {rutas[3] = layer});
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r3m", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas5"}, { glowColor: "rgb( 255, 62, 0)", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r3m.geojson", function(layer) {rutas[4] = layer});
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r3t", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas6"}, { glowColor: "rgb(  64, 77, 236 )", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r3t.geojson", function(layer) {rutas[5] = layer});
                 
             rutas[rutas.length] = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Limpia r3tr", layerType: LayerType.DINAMIC, selectable: true,
                     id: "LRutas6"}, { glowColor: "rgb(  225, 64, 236 )", lineWidth: 8}, "data/LIMPIA_PUBLICA2/r3tr.geojson", function(layer) {rutas[6] = layer});
                 
           // }
            
            /*limpiaRutasTR = LayerFactory.createMemoryLayer(referenceC, {label: "Rutas Limpia", layerType: LayerType.DINAMIC, selectable: true,
                painter: new CamionPainter(), id: "LRutasTR"}, "data/LIMPIA_PUBLICA/rutasLimpia.geojson");
            map.layerTree.addChild(limpiaRutasTR);
            limpiaRutasTR.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };
            
            limpiaCamiones = LayerFactory.createMemoryLayer(referenceC, {label: "Camiones", layerType: LayerType.DINAMIC, selectable: true, 
                painter: new CamionPainter(), id: "LCamiones"});
            map.layerTree.addChild(limpiaCamiones);
            limpiaCamiones.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };
            */
            //startSimulationLimpia();
            //var limpiaView = LayerFactory.createFeatureLayer(referenceC, {label:"Limpia View", layerType: LayerType.STATIC, id: "LimpiaView"}, "data/LIMPIA_PUBLICA/rutasLimpia.geojson");
            //map.layerTree.addChild(limpiaView);
            //map.layerTree.addChild(limpiaRutas);
        }
        // ================================ optibus ===================================== //
        if(tipoUsuario === "dev" || tipoUsuario === "movilidad"){
            var optibusHandeler = function (layer) { 
                optibusRuta = layer; 
                /*optibusRuta.balloonContentProvider = function (feature) {
                    return DefaultBalloon.getBalloon(feature);
                 };*/
            };
            optibusRuta = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Optibus", layerType: LayerType.DINAMIC, selectable: true, id: "MOptibusRutas"}, 
                {dataSetStartTime: Date.parse("2019-12-15 08:00"), dataSetEndTime: Date.parse("2019-12-15 18:00"), 
                    glowColor: "rgb( 0, 252, 27 )", lineWidth: 10}, "data/optibus/optibus2(1).geojson", optibusHandeler);
            var optibusHandeler2 = function (layer) { optibusRuta2 = layer; };
            /*optibusRuta2 = LayerFactory.createGlowSlopesLayerData(referenceC, map, {label: "Rutas Optibus", layerType: LayerType.DINAMIC, selectable: true, id: "OptibusRutas2"}, 
                {dataSetStartTime: Date.parse("2019-12-15 08:00"), dataSetEndTime: Date.parse("2019-12-15 18:00"), 
                    glowColor: "rgb( 0, 252, 27 )", lineWidth: 10}, "data/OPTIBUS.geojson", optibusHandeler2);
            */
            optibusView = LayerFactory.createFeatureLayer(referenceC, {label:"Optibus View", layerType: LayerType.STATIC, id: "MOptibusView"}, "data/OPTIBUS.geojson");
            map.layerTree.addChild(optibusView);
            
            
            optibusEstaciones = LayerFactory.createFeatureLayer(referenceC, {label:"Estaciones Optibus", layerType: LayerType.STATIC, id:"MOptibusEstaciones",
                painter: new EstacionesPainter(), selectable: true}, "data/optibus/estaciones.geojson");
            map.layerTree.addChild(optibusEstaciones);
            optibusEstaciones.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };
            
            GraficasOptibus.crearTabla();
            GraficasOptibus.crearGraficasEstaciones();
            
            semaforosCalles = LayerFactory.createFeatureLayer(referenceC, {label: "Ruta Semaforos", layerType: LayerType.STATIC, selectable: true,
                painter: new SemaforosCallesPainter(), id: "MSemaforosCalles"}, "data/SEMAFOROS/crucero.json");
            map.layerTree.addChild(semaforosCalles);
            semaforosCalles.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };
            
            semaforos = LayerFactory.createFeatureLayer(referenceC, {label: "Semaforos", layerType: LayerType.STATIC,
                painter: new SemaforosPainter(), selectable: true, id: "MSemaforos"}, "data/SEMAFOROS/CRUCEROS.json");
            map.layerTree.addChild(semaforos);
            /*semaforos.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };*/
        }
        
        // ================================ obras ===================================== //
        if(tipoUsuario === "dev" || tipoUsuario === "obras") {
            
            agebs = LayerFactory.createFeatureLayer(referenceC, {label: "AGEBS", selectable:true, layerType: LayerType.DINAMIC, 
                painter: new AgebPainter(), id: "OAgebs"}, "data/Base/ageb.geojson");
            map.layerTree.addChild(agebs);
            agebs.balloonContentProvider = function (feature) {
               return AgebBalloon.getBalloon(feature);
            };
            
            secciones = LayerFactory.createFeatureLayer(referenceC, {label: "Secciones", layerType: LayerType.DINAMIC, painter: new SeccionesPainter(),
                id: "OSecciones", selectable: true}, "data/seccionest.geojson");
            map.layerTree.addChild(secciones);
            secciones.balloonContentProvider = function (feature) {
               return SeccionBalloon.getBalloon(feature);
            };
            
            obrasPublicas = LayerFactory.createFeatureLayer(referenceC, {label:"Obras Publicas", layerType: LayerType.DINAMIC, 
                painter: new heatPainter(), selectable: true, id: "ObrasPublicas"}, "data/obras/obras_final.geojson");
            map.layerTree.addChild(obrasPublicas);
            obrasPublicas.balloonContentProvider = function (feature) {
               return DefaultBalloon.getBalloon(feature);
            };
            sincTimeLine(obrasPublicas);
            
        }
    }
    Start();
    
    /*
     * ===========================================================================================
     *                          Reloj
     * ===========================================================================================
     */
    Clock();
    function Clock() {
        //$('#timelabel').text(Util.formatDate(null, "dd/mm/aaaa hh:mm"));
        /*sMovilidad.filter = function (feature) {
            return true;
        };*/
        try {
            agebs.filter = function (feature) {
                return true;
            };
            
            var sector = document.getElementById("partidosSecciones").selectedOptions[0].innerHTML;
            if(sector === "Obras") {
                secciones.filter = function (feature) { return true; };
            }
        } catch(e) {}
        
        setTimeout(Clock,1000);
    }
    
    /*
     * ===========================================================================================
     *                          Map Selector
     * ===========================================================================================
     */
    var selectedFeature;
    map.on("SelectionChanged", function (event) {
            var selectedFeature = null;
            if (event.selectionChanges.length > 0 && event.selectionChanges[0].selected.length > 0 ) {
                selectedFeature = event.selectionChanges[0].selected[0]; 
                
                if(event.selectionChanges[0].layer.label === 'Rutas Optibus') {
                    $("#botonOptibus").fadeIn();
                    $("#botonSemaforos").fadeOut();
                    $("#balloonSensores").fadeIn("slow");
                    var balloon = DefaultBalloon.getBalloon(selectedFeature);
                    document.getElementById("balloonContent").innerHTML = balloon;
                }
                
                if(event.selectionChanges[0].layer.label === 'Semaforos') {
                    $("#botonOptibus").fadeOut();
                    $("#botonSemaforos").fadeIn();
                    $("#balloonSensores").fadeIn("slow");
                    var balloon = DefaultBalloon.getBalloon(selectedFeature);
                    document.getElementById("balloonContent").innerHTML = balloon;
                }
            } else {
                $("#balloonSensores").fadeOut("slow");
                //mostrarOcultarSensores();
            }
        });
        /*map.on("MapChange", function (event) {
            console.log("X "+map.xScale+"    Y "+map.yScale);
            actualizarS = false;
        });
    /*
     * ===========================================================================================
     *                          Inicio de sensores
     * ===========================================================================================
     */
    function mostrarLeftPanel(div) {
        $("#"+div).fadeIn();
    }
    function ocultarLeftPanel(div) {
        $("#"+div).fadeOut();
    }
    
    
    /*
     *== ==========================================================================================================
     *                                      cambio en el mapa
     *============================================================================================================
     */
    var roadL, arealL, world;
    $("#cambiarMapa").click(function () {
        if(!roadL || !arealL) {
            var Layers = map.layerTree.children;
            for(var l in Layers) {
                var id = Layers[l].id;
                if(id === "RoadBing")
                    roadL = Layers[l];
                if(id === "AerialBing" || id === "AerialWithLabelsBing")
                    arealL = Layers[l];
                if(id === "World")
                 world = Layers[l];
            }
        }
        if(roadL)
            roadL.visible = !roadL.visible;
        if(arealL)
            arealL.visible = !roadL.visible;
    });
    /*
     *============================================================================================================
     *                                      Filtro de infraestructura
     *============================================================================================================
     *
    var lum = ["lumCentro","lumNorte","lumSur","lumPoniente","lumOriente"];
    function updateLuminarias() {
        luminarias.filter = function() { return true; };
    }
    lum.forEach(function (div) {
        $("#"+div).on("input change", function () {
            updateLuminarias();
        });
    });
    
    /*
     *============================================================================================================
     *                                      Listeners
     *============================================================================================================
     */
    
    $("#capaObras").on("input change", function (div) {
        var check = div.currentTarget.checked;
        obrasPublicas.visible = check;
    });
    $("#capaAgeb").on("input change", function (div) {
        var check = div.currentTarget.checked;
        agebs.visible = check;
    });
    $("#capaSemaforos").on("input change", function (div) {
        var check = div.currentTarget.checked;
        semaforos.visible = check;
    });
    $("#capaSemaforosCalles").on("input change", function (div) {
        var check = div.currentTarget.checked;
        semaforosCalles.visible = check;
    });
    $("#capaOptibus").on("input change", function (div) {
        var check = div.currentTarget.checked;
        optibusRuta.visible = check;
    });
    $("#capaOptibusView").on("input change", function (div) {
        var check = div.currentTarget.checked;
        optibusView.visible = check;
    });
    $("#capaOptibus2").on("input change", function (div) {
        var check = div.currentTarget.checked;
        optibusRuta2.visible = check;
    });
    $("#capaLimpiaRutas").on("input change", function (div) {
        var check = div.currentTarget.checked;
        limpiaRutas.visible = check;
        rutas.forEach(function(layer) {
            layer.visible = check;
        });
    });
    $("#capaOrigenDestino").on("input change", function (div) {
        var check = div.currentTarget.checked;
        limpiaUbicaciones.visible = check;
    });
    $("#capaCamiones").on("input change", function (div) {
        var check = div.currentTarget.checked;
        limpiaCamiones.visible = check;
    });
    $("#capaHistorialCamiones").on("input change", function (div) {
        var check = div.currentTarget.checked;
        limpiaRutasTR.visible = check;
    });
    $("#capaSecciones").on("input change", function (div) {
        var check = div.currentTarget.checked;
        secciones.visible = check;
    });
    $("#capaEstaciones").on("input change", function (div) {
        var check = div.currentTarget.checked;
        optibusEstaciones.visible = check;
    });
    var sector=0;
    limpiaSectores.forEach(function (layer) {
        $("#capaSector"+sector).on("input change", function (div) {
            var check = div.currentTarget.checked;
            layer.visible = check;
        });
        sector++;
    });
    var etiquetas = true;
    $("#Labels").click(function () {
        etiquetas = !etiquetas;
        var layers = map.layerTree.children;
        layers.forEach(function (layer) {
            if(layer.type !== LayerType.BASE && layer.label !== "Grid") {
                Util.setLabels(layer, etiquetas);
            }
        });
    });
    /*
     *============================================================================================================
     *                                      Crear Capa Eventos
     *============================================================================================================
     */
    function sincTimeLine(layer) {
        var queryFinishedHandleMexico = layer.workingSet.on("QueryFinished", function() {
            queryFinishedHandleMexico.remove();
            var modelo = layer.model;
            
            var eventosPromise = layer.model.query();
            Promise.when(eventosPromise, function (cursor) {
                var keyEventTime = "Fecha de I";
                var features = [];
                var index = 0;
                var startDate, endDate;
                while (cursor.hasNext()) {
                    var feature = cursor.next();
                    var time = feature.properties[keyEventTime];
                    feature.properties.EventTime = time;
                    features[index] = feature;
                    index++;
                    time = Date.parse(time);
                    if(!startDate) 
                        startDate = time;
                    if(!endDate)
                        endDate = time;
                    if(time < startDate)
                        startDate = time;
                    if(time > endDate) 
                        endDate = time;
                }
                actualTimeObras = startDate;
                document.getElementById("nEventos").innerHTML = index;
                var lab = ["Ciclo", "Periodo", "SECCION","CVE_AGEB", "Categoria", "Contratist", "Ramo"];
                var fil = [true, true, true, true, true, true, true];
                var titles = ["Ciclo", "Periodo", "SECCION","AGEB", "Categoria", "Contratista", "Ramo"];
                var tops= [10, 10, 10, 10, 10, 10, 10];
                var types = ["piechart"];
                var pieOptions ={width: 400, height: 220,
                    titleTextStyle: {
                        color: '#FFFFFF',
                        frontSize: 20
                      },
                    legend: {
                      textStyle: {
                          fontSize: 11,
                          color: "#95a5a6"
                      }
                    },
                    backgroundColor:{
                        fill: "#17202a"
                    },
                    chartArea: {
                        left: 10,
                        width: 390,
                        height: 210,
                        backgroundColor: "#17202a"
                    },
                    pieSliceBorderColor: "#17202a"};
                var tablaDatos = Tablas.crearArreglo(features);
                var datos = Graficas.crearGraficasFeatures(tablaDatos, "graficasObrasPublicas", lab, types, fil, null, titles, null, tops, pieOptions, true);
                var filOptions = {filtros: datos.filtros, labels: lab};
                var startTime = startDate/1000, endTime = endDate/1000;
                try {
                    updateHistogram.updateHistogram(map, features, layer, referenceC, startTime, endTime, datos.tablaDatos, datos.graficas, filOptions, keyEventTime);
                    //$("#afterafter").fadeOut("slow");
                } catch(e) {
                    console.error(e);
                }
                
                var timeChartOptions3 = {
                    startTime: Util.formatDate(startDate, "aaaa/mm/dd hh:mm"), endTime: Util.formatDate(endDate, "aaaa/mm/dd hh:mm"), speedUp: 2000000, playing: false,
                    divPlay: "playTimeObras", format: "aaaa/mm/dd"
                };
                var timeChartObras = TimeChartManager.startTimeChart("timeChartObras", map, timeChartOptions3, function () {
                    try {
                        var time = timeChartObras.getCurrentTime().getTime();
                        actualTimeObras = time;
                        $('#timeLabelObras').text(Util.formatDate(time, "aaaa/mm/dd"));
                        var sector =  Math.round(parseFloat($("#Densidad").val()));
                        if(sector >10) 
                            obrasPublicas.filter = obrasPublicas.filter;
                    } catch(e) {
                    }
                });
            });
        });
    }
    /*
     *============================================================================================================
     *                                      Control Densidad
     *============================================================================================================
     */
    $("#Densidad").on("change input", function() {
        var x = 1, y = 0;
        x = Math.round(parseFloat($("#Densidad").val()));
        if(x<=10 && x >0) {
            obrasPublicas.painter.density = {
                colorMap: ColorMap.createGradientColorMap([
                {level: y, color: "rgba(  0,   0,   255, 1)"},
                {level: y+=x, color: "rgba(  0, 100,   255, 1)"},
                {level: y+=x*2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: y+=x*3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: y+=x*4, color: "rgba(255, 0, 0, 1.0)"}
                ])
            };
        } else {
            obrasPublicas.painter.density = null;
        }
    });
    
    $("#lineaTiempo").on("change input", function(div) {
        var check = div.currentTarget.checked;
        if(check===true) 
            $("#afterafter").fadeIn("slow");
        else
            $("#afterafter").fadeOut("slow");
    });
    /*
     *============================================================================================================
     *                                      TimeChart Rutas Limpia
     *============================================================================================================
     */
    var timeChart2, min, actualizarS=false;
    function actualizarRutas() {
        
        try {
            actualTime = timeChart2.getCurrentTime().getTime();
            limpiaRutas.painter.timeWindow = [actualTime - 5000000, actualTime];
            optibusRuta.painter.timeWindow = [actualTime - 1000000, actualTime];
            //optibusRuta2.painter.timeWindow = [actualTime - 1000000, actualTime];
            var dif;
            if(actualTime >= Date.parse("2019/12/15 22:59"))
                min = Date.parse("2019/12/15 05:00");
            if(!min) 
                min = actualTime;
            dif = actualTime - min;
            if(dif < 0)
                dif = dif*(-1);
            if(dif > 60000*10) {
                dif = 0;
                min = actualTime;
                var updatetime = actualTime;
                semaforos.filter = function(feature) {
                    feature.properties.lastUpdate = feature.properties.lastUpdate || updatetime;
                    var d = updatetime - feature.properties.lastUpdate;
                        if(d < 0)
                            d = d*(-1);
                    if(d > 60000*10) {
                        
                        feature.properties.estado++;
                        if(feature.properties.estado >=4)
                            feature.properties.estado = 0;
                        feature.properties.lastUpdate = updatetime;
                    }
                    return true;
                };
                semaforosCalles.filter = function (feature) {
                    feature.properties.lastUpdate = feature.properties.lastUpdate || updatetime;
                        var d = updatetime - feature.properties.lastUpdate;
                        if(d < 0)
                            d = d*(-1);
                        if(d > 60000*10) {
                            if(!feature.properties.estado) {
                                if(feature.properties.estado !== 0)
                                    feature.properties.estado = 0;
                                else
                                    feature.properties.estado++;
                            } else
                                feature.properties.estado++;
                            if(feature.properties.estado >=4)
                                feature.properties.estado = 1;
                            feature.properties.lastUpdate = updatetime;
                        }
                        return true;
                };
            }
            GraficasOptibus.crearTabla();
            GraficasOptibus.crearGraficasEstaciones();
            
            limpiaSectores.forEach(function (layer) {
                    layer.filter = function (feature) { return true; };
                });
            
            rutas.forEach(function (layer) {
               layer.painter.timeWindow = [actualTime - 1000000, actualTime]; 
            });
            
            optibusEstaciones.filter = function () { return true; };
        } catch(e){};
        $('#timeLabel').text(Util.formatDate(actualTime, "aaaa/mm/dd hh:mm"));
     }
    
    function rutasHandeler(layer, features) {
        limpiaRutas = layer;
        var timeChartOptions2 = {
            startTime: "2019-12-15 05:00", endTime: "2019-12-15 23:00" , speedUp: 50, playing: true,
            divPlay: "playTimeLimpia", format: "hh:mm"
        };
        timeChart2 = TimeChartManager.startTimeChart("timeChartLimpia", map, timeChartOptions2, actualizarRutas);
    }
    /*
     *============================================================================================================
     *                                      Control Panel Izquierdo
     *============================================================================================================
     */
    var paneles = ["Limpia", "Movilidad", "Obras"];
    paneles.forEach(function (panel) {
        $("#show"+panel).click(function() {
            cambiarPanelIzquierdo(panel);
        });
    });
    function cambiarPanelIzquierdo(div) {
        for(var i in paneles) {
            if(div === paneles[i])
                $("#LP"+div).fadeIn("slow");
            else
                $("#LP"+paneles[i]).fadeOut();
        }
        setMapTo(div);
        if(div === "Obras")
            $("#afterafter").fadeIn("slow");
        else
            $("#afterafter").fadeOut("slow");
    }
    function setMapTo(usuario) {
        var layers = map.layerTree.children;
        layers.forEach(function (layer) {
           if(layer.type !== LayerType.BASE) {
               var x=false;
               var id = layer.id;
               id = id.charAt(0);
               if(usuario === "Limpia" && id === "L" && x===false) {
                   layer.visible = true;
                   x=true;
               }
               
               if(usuario === "Movilidad" && id === "M" && x===false) {
                   layer.visible = true;
                   x=true;
               }
               
               if(usuario === "Obras" && id === "O" && x===false) {
                   layer.visible = true;
                   x=true;
               }
               if(x===false && layer.label !== "Grid")
                   layer.visible = false;
               
           } 
        });
        
        switch(usuario) {
            case "Limpia":
                 map.mapNavigator.fit({bounds: limpiaRutas.bounds, animate: true});
                 break;
            case "Movilidad":
                map.mapNavigator.fit({bounds: optibusRuta.bounds, animate: true});
                break;
            case "Obras":
                 map.mapNavigator.fit({bounds: obrasPublicas.bounds, animate: true});
                 break;
        }
        
    }  
    
    
    $("#descargarTabla1").click(function (element) {
        Util.exportarTablaXLS(element, "tablaCompleta1");
    });
    
    $(".draggable").draggable({ cancel: ".panelContent", grid: [ 10, 10 ] });
    /*
     *============================================================================================================
     *                                      Control Actuador
     *============================================================================================================
     */
    // rutas = Array de coordenadas a seguir
    var rutas=[], posiciones=[];
    var coordenadaInicio = [-101.691948899829995, 21.09704669977889];
    var coordenadaFinal = [-101.776807169722005, 21.174422087904329];
    var playing = document.getElementById("playTimeLimpia").checked;
    var updater=[];
    var startTime= Date.parse("2019/12/14 08:00"), endTime= Date.parse("2019/12/14 18:00"); 
    function moverCamion(id, trayectoria) {
       // if(rutas[id]) {
           // var trayectoria = rutas[id];
            var posicion = posiciones[id];
            var limite = trayectoria.length;
            if(posicion < limite) {
                var lon = trayectoria[posicion][0];
                var lat = trayectoria[posicion][1];
                var feature = limpiaCamiones.model.get(id);
                var feature = Shapes.createPoint(referenceC, lon, lat, 0, feature.id, feature.properties);
                limpiaCamiones.model.put(feature);
                actualizarRuta(id, posiciones[id]);
                posiciones[id]++;
            } else {
                //estadoPatrullas[id] = 0;
                limpiaRutas.model.remove(id);
                //efectos.model.remove(id);
                //console.log("Estado Patrulla "+id+": "+0);
            }
        //}
    }
    
    function actualizarRuta(id, limite) {
        var nuevaRuta, nuevasX = new Array(), nuevasY = new Array();
        var ruta = limpiaRutasTR.model.get(id), coordenadas = rutas[id];
        //console.log(ruta);
        //nuevasX[0] = coordenadas[coordenadas.length-1][0];
        //nuevasY[0] = coordenadas[coordenadas.length-1][1];
        var k=0;
        for(var i=0; i<=limite; i++) {
            nuevasX[k] = coordenadas[i][0];
            nuevasY[k] = coordenadas[i][1];
            k++;
        }
        nuevaRuta = Shapes.createPolyline(referenceC, nuevasX, nuevasY, 0, id, ruta.properties);
        limpiaRutasTR.model.put(nuevaRuta);
    }
    
    function updateCamion(id, nPuntos) {
        var intervalo = (endTime - startTime) / nPuntos;
        updater[id] = setInterval(function () {
            if(playing===true) {
                //for(var i in rutas) {
                    moverCamion(id, rutas[id]);
                //}
            } else 
                clearInterval(updater[id]);
        }, intervalo);
    }
    
    function startSimulationLimpia() {
        coordenadaInicio = [-101.691948899829995, 21.09704669977889];
        coordenadaFinal = [-101.776807169722005, 21.174422087904329];
        
        $.getJSON("data/LIMPIA_PUBLICA/rutasLimpia.geojson", function (data) {
            var features = data.features;
            for(var i in features) {
                rutas[i] = features[i].geometry.coordinates;
                posiciones[i] = 0;
                var x = coordenadaInicio[0];
                var camion = Shapes.createPoint(referenceC, coordenadaInicio[0], coordenadaInicio[1], 0, i,
                    {name: "Camion "+1, conductor: "Pedro", idCamion: Util.getRandom(1000, 10000)});
                limpiaCamiones.model.add(camion);
                updateCamion(i, rutas[i].length);
            }
        }).fail(function (e) {
            console.error(e);
        });
    }
    var actualizar= true;
    $("#playTimeLimpia").on("change input", function (element) {
        if(actualizar === true) {
            actualizar = false;
            playing = element.currentTarget.checked;
            updateCamion();
        } else
            actualizar = true;
    });
    /*
     *============================================================================================================
     *                                      Control Actuador
     *============================================================================================================
     */
    
    function crearTablaContenedores() {
        $.getJSON("data/LIMPIA_PUBLICA2/data.json", function (datos) {
            var tabla = [["Contenedor", "Fecha", "Peso", "Volumen", "Estado Peso", "Estado Volumen"]];
            var n = datos["contenedor"].length;
            var tablaGrafica=[["Fecha", "Peso", "Volumen"]];
            for(var i=0; i<n; i++) {
                tabla[tabla.length] = [datos.contenedor[i], datos.fecha[i], datos.peso[i], datos.volumen[i], datos.estadopeso[i], datos.estadovolumen[i]];
            }
            datosContenedores = datos;
            var name = "c6.1";
            for(i=0; i<n; i++) {
                if(name !== datos.contenedor[i]) {
                    areaOptions.title = name;
                    AreaChart("Grafica"+name, tablaGrafica);
                    tablaGrafica = [["Fecha", "Peso", "Volumen"]];
                    name = datos.contenedor[i];
                }
                tablaGrafica[tablaGrafica.length] = [new Date(datos.fecha[i]), datos.peso[i], datos.volumen[i]];
            }
            AreaChart("Grafica"+name, tablaGrafica);
            Util.crearTablaDIV("tablaContenedores", tabla );
        }).fail(function (e) {
            console.error(e);
        });
        
        $.getJSON("data/optibus/data_estaciones.json", function (data) {
            datosEstaciones =data;
            var n = data.Name.length;
            var tablas={};
            var tabla = [["Nombre", "Fecha","Cantidad","Estado"]];
            for(var i=0; i<n; i++) {
                tabla[tabla.length] = [data.Name, data.fecha, data.contenedor, data.estado];
            }
            for(i=0; i<n; i++) {
                var name = data.Name[i];
                if(!tablas[name]) {
                    tablas[name] = [["Fecha", "Cantidad"]];
                }
                tablas[name][tablas[name].length] = [new Date(data.fecha[i]), data.cantidad[i]];
            }
            
            for(i in tablas) {
                AreaChart("grafica"+i, tablas[i]);
            }
        }).fail(function (e) {
            console.log(e);
        });
    }
    crearTablaContenedores();
    var leftWidth = $("#left").innerWidth();
    var areaOptions = { title: "algo",  width: leftWidth - 50, height: 250,
                legend: { position: "bottom", textStyle: {color: '#95a5a6'} },
                backgroundColor:{ fill: "#17202a" },
                chartArea: {  top: 5, backgroundColor: "#17202a"  },
                animation:{ duration: 1000, easing: 'linear' },
                hAxis: { 
                    //logScale: false, slantedText: true, format: 'short',
                  textStyle: { color: '#FFFFFF', frontSize: 10 },
                  titleTextStyle: { color: '#FFFFFF', frontSize: 16 }
                },
                vAxis: {  direction: 1, textStyle: { color: '#FFFFFF' } }
            };
    function AreaChart(div, datos, id, rt){
        try{
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawAreaChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawAreaChart() {
            var grafica = new google.visualization.AreaChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            
            grafica.draw(datosNuevos, areaOptions);
            
        }
    } 
    
    var partidos = ["PAN", "PRI", "PRD", "PVEM", "PT", "MC", "NA", "MORENA", "ES", "PANcom", "PRIcom", "PRDcom",
        "PVEMcom", "PTcom", "MCcom", "NAcom", "MORENAcom", "EScom", "part", "Obras"];
    Util.setOptions("partidosSecciones", partidos, false);
    $("#partidosSecciones").on("change input", function () {
        secciones.filter = function (feature) { return true; };
    });
    /*
     *============================================================================================================
     *                                      Fin de Main
     *============================================================================================================
     */
});

function getActualTimeObras () {
    return actualTimeObras;
}
function getActualTime() {
    return actualTime;
}

function getDatosContenedores(id) {
    if(!datosContenedores) 
        return null;
    var key, contenedores = datosContenedores.contenedor;
    var time, datos;
    for(var i=0; i<contenedores.length; i++) {
        if(contenedores[i] === id) {
            time = Date.parse(datosContenedores.fecha[i]);
            key = i;
            if(actualTime > time) {
                if(i < contenedores.length-1) {
                    var time2 = Date.parse(datosContenedores.fecha[i+2]);
                    if(actualTime < time2) {
                        datos = {
                            contenedor: id,
                            fecha: datosContenedores.fecha[i],
                            peso: datosContenedores.peso[i],
                            volumne: datosContenedores.volumen[i],
                            estadopeso: datosContenedores.estadopeso[i],
                            estadovolumen: datosContenedores.estadovolumen[i]
                        };
                        break;
                    }
                } else {
                    datos = {
                            contenedor: id,
                            fecha: datosContenedores.fecha[i],
                            peso: datosContenedores.peso[i],
                            volumne: datosContenedores.volumen[i],
                            estadopeso: datosContenedores.estadopeso[i],
                            estadovolumen: datosContenedores.estadovolumen[i]
                        };
                        break;
                }
            }
        }
    }
    if(!datos) {
        if(!key && key !==0)
            return null;
        return {
            contenedor: id,
            fecha: datosContenedores.fecha[key],
            peso: datosContenedores.peso[key],
            volumne: datosContenedores.volumen[key],
            estadopeso: datosContenedores.estadopeso[key],
            estadovolumen: datosContenedores.estadovolumen[key]
        };
    }
    return datos;
}

function getDatosEstaciones(id) {
    if(!datosEstaciones) 
        return null;
    var key, names = datosEstaciones.Name;
    var time, datos;
    for(var i=0; i<names.length; i++) {
        if(names[i] === id) {
            time = Date.parse(datosEstaciones.fecha[i]);
            key = i;
            if(actualTime > time) {
                if(i < names.length-1) {
                    var time2 = Date.parse(datosEstaciones.fecha[i+2]);
                    if(actualTime < time2) {
                        return {
                            name: id,
                            fecha: datosEstaciones.fecha[i],
                            color: datosEstaciones.estado[i]
                        };
                    }
                } 
            } else {
                return {
                            name: id,
                            fecha: datosEstaciones.fecha[i],
                            color: datosEstaciones.estado[i]
                };
            }
        }
    }
    return {};
}
