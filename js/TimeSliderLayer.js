/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define ([
    
    "luciad/model/store/UrlStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "recursos/js/Shapes"
], function (UrlStore, FeatureModel, FeatureLayer, Shapes) {
   
    function createGlowSlopesLayer(reference, layerOptions, glowOptions, urlOrData) {
    //function createGlowSlopesLayer(url, dataSetStartTime, dataSetEndTime, glowColor, label) {
      /*var trajectoriesPainter = new TrajectoryPainter({
        properties: ["origin", "airline", "destination"],
        defaultColor: glowOptions.glowColor || "rgb(255, 255, 255)",
        selectionColor: glowOptions.glowColor || "rgb(255, 255, 255)",
        lineWidth: glowOptions.lineWidth || 3,
        lineType: LineType.SHORTEST_DISTANCE,
        timeWindow: [0, glowOptions.dataSetEndTime - glowOptions.dataSetStartTime],
        timeProvider: function(feature, shape, pointIndex) {
          return feature.properties.timestamps[pointIndex];
        },
        draped: true
      });*/
      //al =0;
      var trajectoryStore;
      if (typeof urlOrData === 'string') {
            trajectoryStore = new UrlStore({target: urlOrData, codec: new GeoJsonCodec()});
        } else {
            trajectoryStore = new MemoryStore({ data: urlOrData });
        }
      //var reference = ReferenceProvider.getReference("CRS:84");
      var trajectoryModel = new FeatureModel(trajectoryStore, {reference: reference});
      layerOptions.painter = layerOptions.painter || trajectoriesPainter;
      return new FeatureLayer(trajectoryModel, layerOptions);
    }
    function createGlowSlopesLayerData(reference, map, layerOptions, timeOptions, url, setLayer) {
    //function createGlowSlopesLayerData(url, dataSetStartTime, dataSetEndTime, glowColor, label, map, x) {
        var features = null, intervalo =0;
        $.ajax({
            type:'Get',
            dataType: "json",
            url: url
        }).done(function(data) {
                console.log("archivo leido");
                var layer;
                if(data.features) {
                    features = Shapes.crearFeatureTime(data.features, ReferenceProvider.getReference("CRS:84"), 
                        timeOptions.dataSetStartTime, timeOptions.dataSetEndTime);
                    console.log("Feature con tiempo creado");
                    layer = createGlowSlopesLayer(reference, layerOptions, timeOptions, features);
                    //layer = createGlowSlopesLayer(features, dataSetStartTime, dataSetEndTime, glowColor, label);
                    map.layerTree.addChild(layer);
                    setLayer(layer, features);
                    
                }
                else {
                    console.log("el archivo no se puede leer, no features");
                    return null;
                }
            }
        ).fail(function(errMsg) {
                console.log("no se leyo el archivo\n" + errMsg);
                return null;
        });
    }
    
});
