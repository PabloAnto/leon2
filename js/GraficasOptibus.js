/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["recursos/js/Util"], function (Util) {
    
    var tablaDatos = [
        ["Nombre","Salida", "Llegada Estimada", "Llegada Real", "Tiempo Recorrido", "Termino a tiempo"],
        ["Juan", "2019/12/15 05:00", "2019-12-15 13:00", "2019-12-15 13:00", "8 hr", "Si"],
        ["Rodrigo", "2019-12-15 05:30", "2019-12-15 13:30","2019-12-15 13:45", "8 hr", "No" ],
        ["Alberto", "2019-12-15 06:00", "2019-12-15 14:00", "2019-12-15 14:00", "8 hr", "Si" ],
        ["Jose", "2019-12-15 07:00", "2019-12-15 15:00", "2019-12-15 15:00", "8 hr", "Si" ],
        
        ["Roberto", "2019/12/15 05:00", "2019-12-15 13:00", "2019-12-15 13:00", "8 hr", "Si"],
        ["Angel", "2019-12-15 05:30", "2019-12-15 13:30", "2019-12-15 13:50", "8 hr", "No" ],
        ["Miguel", "2019-12-15 06:00", "2019-12-15 14:00", "2019-12-15 14:00", "8 hr", "Si" ],
        ["Sergio", "2019-12-15 07:00", "2019-12-15 15:00", "2019-12-15 15:10", "8 hr", "No" ],
        
        ["Beto", "2019/12/15 05:00", "2019-12-15 13:00", "2019-12-15 13:00", "8 hr", "Si"],
        ["Pablo", "2019-12-15 05:30", "2019-12-15 13:30", "2019-12-15 13:30", "8 hr", "Si" ],
        ["Carlos", "2019-12-15 06:00", "2019-12-15 14:00", "2019-12-15 14:00", "8 hr", "Si" ],
        ["Sebastian", "2019-12-15 07:00", "2019-12-15 15:00", "2019-12-15 15:00", "8 hr", "Si" ],
        
        ["Alan", "2019/12/15 05:00", "2019-12-15 13:00", "2019-12-15 13:00", "8 hr", "Si"],
        ["Jensen", "2019-12-15 05:30", "2019-12-15 13:30", "2019-12-15 13:40", "8 hr", "No" ],
        ["Alexis", "2019-12-15 06:00", "2019-12-15 14:00", "2019-12-15 14:00", "8 hr", "Si" ],
        ["Adrian", "2019-12-15 07:00", "2019-12-15 15:00", "2019-12-15 15:30", "8 hr", "No" ]
    ];
    
    function crearTabla () {
        var tabla = [["Nombre","Salida", "Llegada Estimada", "Llegada Real", "Tiempo Recorrido", "Termino a tiempo"]];
        var tiempoActual = getActualTime();
        for(var i=1; i<tablaDatos.length; i++) {
            var salida = Date.parse(tablaDatos[i][1]);
            if(salida <= tiempoActual) {
                var llegada = Date.parse(tablaDatos[i][2]);
                if(llegada < tiempoActual) {
                    tabla[tabla.length] = tablaDatos[i];
                } else
                    tabla[tabla.length] = [tablaDatos[i][0],tablaDatos[i][1],tablaDatos[i][2], "", "", "En recorrido"];
            }
        }
        
        Util.crearTablaDIV("TablaOptibus", tabla);
    }
    var eComparativa;
    var tablaEstaciones = [
        ["fecha", "verde", "amarillo", "naranja", "rojo"],
        ["2019-12-15 05:00:00", 1,1,6,4],
        ["2019-12-15 06:08:34", 1,1,6,4],
        ["2019-12-15 07:17:08", 0,2,6,3],
        ["2019-12-15 08:25:42", 0,1,6,4],
        ["2019-12-15 09:34:17", 1,1,6,4],
        ["2019-12-15 10:42:51", 1,2,3,4],
        ["2019-12-15 11:51:25", 1,2,4,3],
        ["2019-12-15 12:00:00", 0,3,3,3]];
    
    function crearGraficasEstaciones() {
        var tiempoActual = getActualTime();
        var names = datos.Name, n = tablaEstaciones.length;
        var tabla = [["Color", "Cantidad", {role: "style"}]];
        var cantidades = {};
        for(var i=0; i<n; i++) {
            var fecha = Date.parse(tablaEstaciones[i][0]);
            if(tiempoActual > fecha) {
                tabla[1] = ["Verde", tablaEstaciones[i][1] || 0, "green"];
                tabla[2] = ["Amarillo", tablaEstaciones[i][2]  || 0, "yellow"];
                tabla[3] = ["Naranja", tablaEstaciones[i][3]  || 0, "orange"];
                tabla[4] = ["Rojo", tablaEstaciones[i][4] || 0, "red"];
            }
        }
        if(!eComparativa)
            ColumnChart("graficaEstacionesComparativa", tabla);
        else {
            var datosNuevos = new google.visualization.arrayToDataTable(tabla);
            eComparativa.draw(datosNuevos, columnsOptions);
        }
        
    }
    
    var tablaCalles = [//["fecha", "verde", "amarillo", "naranja", "rojo"],
        ["2019-12-15 05:00:00", 6,4],
        ["2019-12-15 06:08:34", 6,4],
        ["2019-12-15 07:17:08", 6,3],
        ["2019-12-15 08:25:42", 6,4],
        ["2019-12-15 09:34:17", 6,4],
        ["2019-12-15 10:42:51", 3,4],
        ["2019-12-15 11:51:25", 4,3],
        ["2019-12-15 12:00:00", 3,3]];
    
    var left = $("#left");
    var leftWidth = left.innerWidth() - 50;
    leftWidth = leftWidth<300? 300: leftWidth;
    const columnsOptions = { title : "Estaciones", width: leftWidth, height: 280, //orientation: "vertical",
                backgroundColor:{ fill: "#17202a" },
                //legend: { position: "botom" },
                titleTextStyle: { color: '#FFFFFF', frontSize: 30 },
                chartArea: {  backgroundColor: "#17202a" },
                annotations: { textStyle: { fontSize: 12, color: '#FFFFFF', auraColor: 'none' } },
                axisTitlesPosition: "out",
                hAxis: {  logScale: false, slantedText: false, format: 'short',
                  textStyle: { color: '#FFFFFF', frontSize: 10 },
                  titleTextStyle: { color: '#FFFFFF', frontSize: 16 }
                },
                vAxis: { direction: 1, textStyle: { color: '#FFFFFF' } },
                seriesType: 'bars'
                //series: { 1: {type: 'line'} }
                };
    function ColumnChart(div, datos) {
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            return google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n");
            console.log(e);
        }
        function drawColumnChart() {
            eComparativa = new google.visualization.ColumnChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            eComparativa.draw(datosNuevos, columnsOptions);
        }
    }
    
    return {
        crearTabla: crearTabla,
        crearGraficasEstaciones: crearGraficasEstaciones
    };
});
